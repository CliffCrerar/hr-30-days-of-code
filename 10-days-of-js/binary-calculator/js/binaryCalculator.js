(function (win, doc) {
    // Get element by id shorthand function

    let feedback = getEl('fb');
    let Input = getEl('res');
    let errorEl = getEl('error');
    let lastInput = 'none';
    let numberElement;
    let operatorElement;
    const span = createEl('span');
    const calcFn = {
        btn0: () => inputNumber(0),
        btn1: () => inputNumber(1),
        btnClr: () => clear(),
        btnEql: () => calculate(),
        btnSum: () => inputOperator('+'),
        btnSub: () => inputOperator('-'),
        btnMul: () => inputOperator('*'),
        btnDiv: () => inputOperator('/')
    }
    const handleClick = (e) => calcFn[e.target.id]();
    function getEl(id) { return doc.getElementById(id); };
    function createEl(elementType) { return doc.createElement(elementType); };
    function cloneEl(el) { return el.cloneNode(false) };
    async function inputNumber(i) {
        clearError()
        if (['operator', 'none'].includes(lastInput)) {
            numberElement = cloneEl(span);
            numberElement.classList.add('calcEl');
            Input.appendChild(numberElement);
        }
        lastInput = 'number';
        numberElement.innerHTML += i
    }
    function inputOperator(i) {
        clearError()
        try {
            if (['operator', 'none'].includes(lastInput)) {
                throw new Error('<br/> - You cannot start a calculation with this character. <br/> - Nor place two operators next to each other');
            } else if (['number'].includes(lastInput)) {
                operatorElement = cloneEl(span)
                operatorElement.classList.add('calcEl');
                Input.appendChild(operatorElement);
            }
            lastInput = 'operator';
            operatorElement.innerHTML = i
        }
        catch (err) {
            showError(err)
        }
    }
    function calculate() {
        let calculated = 0;
        let calcArr = []
        doc.querySelectorAll('.calcEl').forEach(e => calcArr.push(e.innerHTML));
        // console.log('calcArr: ', calcArr);
        switch (calcArr[1]) {
            case '+': calculated = parseInt(calcArr[0],2) + parseInt(calcArr[2],2); break;
            case '-': calculated = parseInt(calcArr[0],2) - parseInt(calcArr[2],2); break;
            case '*': calculated = parseInt(calcArr[0],2) * parseInt(calcArr[2],2); break;
            case '/': calculated = parseInt(calcArr[0],2) / parseInt(calcArr[2],2); break;
        }
        // console.log('calculated: ', calculated);
        Input.innerHTML = <span>Math.floor(calculated).toString(2)</span>;
    }
    function clear() { doc.querySelectorAll('.calcEl').forEach(e => e.remove()); lastInput = 'none' };
    function showError(msg) { return errorEl.innerHTML = msg; };
    function clearError() { return errorEl.innerHTML = ''; };
    getEl('btns').addEventListener('click', handleClick);
})(window, document)
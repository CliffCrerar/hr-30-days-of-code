﻿using System;
using System.Collections.Generic;
using System.IO;

class Solution {
    static void Main(String[] args) {
        int i = 4;
        double d = 4.0;
        string s = "HackerRank ";


// Declare second integer, double, and String variables.
        // DecimalFormat decFormat = new DecimalFormat('#0.00')
        int i1;
        double d1;
        string s1;
        // Read and save an integer, double, and String to your variables.
        i1 = Convert.ToInt32(Console.ReadLine());
        d1 = Convert.ToDouble(Console.ReadLine());
        s1 = Console.ReadLine();
        // Print the sum of both integer variables on a new line.
        Console.WriteLine(i1+i);
        // Print the sum of the double variables on a new line.
        Console.WriteLine((d1+d).ToString("0.00"));
        // Concatenate and print the String variables on a new line
        // The 's' variable above should be printed first.
        Console.WriteLine(s+s1);

    }
}
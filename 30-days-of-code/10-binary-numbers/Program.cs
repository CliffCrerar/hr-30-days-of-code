﻿using System;

namespace _10_binary_numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int longest = 0;
            foreach(string item in Convert.ToString(n, 2).Split('0'))
            {
                longest = item.Length > longest ? item.Length : longest;
            }
            Console.WriteLine(longest);
        }
    }
}

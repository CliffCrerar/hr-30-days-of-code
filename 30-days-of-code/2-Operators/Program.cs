﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

class Solution
{

        // Complete the solve function below.
        static void solve (double meal_cost, int tip_percent, int tax_percent)
        {
            double tip = Percent(meal_cost,tip_percent);
            // double tax_percent
            double tax = Percent(meal_cost,tax_percent);

            double totalCost = meal_cost + tip + tax;

            //Console.WriteLine(totalCost);
            Console.WriteLine(round(totalCost));
        }   

        static double Percent (double amt, double percent)
        {
            return amt * (percent/100);
        }

        static string round(double amt){
            return amt.ToString("0");
        }

        static void Main (string[] args)
        {
            Console.WriteLine("Enter Meal Cost");
            double meal_cost = Convert.ToDouble (Console.ReadLine ());
            Console.WriteLine("Enter Tip Percent");
            int tip_percent = Convert.ToInt32 (Console.ReadLine ());
            Console.WriteLine("Enter Tax Percent");
            int tax_percent = Convert.ToInt32 (Console.ReadLine ());

            solve (meal_cost, tip_percent, tax_percent);
        }
}
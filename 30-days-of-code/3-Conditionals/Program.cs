﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

class Solution
{

    static void Main (string[] args)
    {
        int N = Convert.ToInt16(Convert.ToDouble(Console.ReadLine()).ToString("0"));
        
        
            // dynamic N = Console.ReadLine (); //.ToString("0.00");
            // Type type = N.GetType ();
            // bool isString = type == typeof (string);
            // if (isString)
            // {
            //     return;
            // }
            // else 
            if (N <= 1 || N >= 101)
            {
                return;
            };
            
            bool even = N % 2 == 0;
            
            print(N >= 6 || N < 21);

            if (even && N >= 6 && N < 21)
            {
                print ("Weird");
            } else {
                return;
            }
            
            if(even)
            {
                print ("Not Weird");
            } else {
                return;
            }

            if(!even)
            {
                print ("Weird");
            } else {
                return;
            }
            return;
    }

    static void print (dynamic param)
    {
        Console.WriteLine (param);
        return;
    }
}
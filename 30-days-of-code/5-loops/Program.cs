﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace HackerRankSolution
{
    class Solution
    {
        static void Main (string[] args)
        {
            int n = Convert.ToInt32 (Console.ReadLine ());
            
            // Loop
            for (int i = 1; i <= 10; i++)
            {
                try // error handling
                {
                    // Test n 
                    if (n < 2 || n > 20)
                    {
                        throw new Exception("Entry must be between 2 and 20!");
                    }
                    // Run if n pass test
                    
                    int result = n*i;
                    print($"{i} x {n} = {result}");
                }
                catch (Exception err) // error handling
                {
                    // Run if n fail test
                    print(err);
                    break;
                }
            }
        }
        // Print out method
        static void print (dynamic toWrite)
        {
            Type type = toWrite.GetType ();
            if (type != typeof (string))
            {
                Console.WriteLine (toWrite.ToString ());
            }
            else
            {
                Console.WriteLine (toWrite);
            }
        }
    }
}
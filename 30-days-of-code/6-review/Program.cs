﻿using System;
using System.Collections.Generic;
using System.IO;
class Solution
{
    static void Main(String [] args)
    {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */
        int T = Convert.ToInt16(Console.ReadLine());
        if (ValidateTestCount(T))
        {
            for (int l = 0; l < T; l++)
            {
                RunTest();
            }
        }

    }

    static void RunTest()
    {
        // Console.WriteLine("Run Test Case");
        dynamic word = Console.ReadLine();
        int wordLength = word.Length;
        ResultSet result = new ResultSet();
        // if(ValidateString(word)){
        //     Console.WriteLine("'Word valid'");
        // } else {
        //     Console.WriteLine("'Word NoT valid'");
        // }
        for (int i = 0; i < wordLength; i++)
        {
            if ((i % 2) == 0)
            {
                result.evenStrings += word [i];
            }
            else
            {
                result.oddStrings += word [i];
            }
        }
        Console.WriteLine($"{result.evenStrings} {result.oddStrings}");
    }

    static bool ValidateTestCount(int T)
    {
        return (T >= 1 && T <= 10);
    }

    static bool ValidateString(dynamic word)
    {
        Type type = word.GetType();
        return !(type == typeof(int) || type == typeof(double));
    }

    public class ResultSet
    {
        public string oddStrings;
        public string evenStrings;
        public ResultSet()
        {
            oddStrings = "";
            evenStrings = "";
        }
    }
}
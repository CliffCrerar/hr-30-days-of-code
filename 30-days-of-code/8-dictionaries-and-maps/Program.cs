﻿using System;
using System.Collections.Generic;
using System.IO;
class Solution
{
    static void Main(String[] args)
    {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution */
        int insert = Convert.ToInt32(Console.ReadLine().Trim());

        int Result;

        Dictionary<string, int> phonebook = new Dictionary<string, int>();

        for (var i = 0; i < insert; i++)
        {
            string[] entry = Console.ReadLine().Split(' ');
            phonebook.Add(entry[0], Convert.ToInt32(entry[1]));
        }
        for (var i = 0; i < insert; i++)
        {
            string keySearch = Console.ReadLine().Trim();
            
            if(phonebook.TryGetValue(keySearch, out Result))
            {
                Console.WriteLine(keySearch + "=" + Result.ToString());
            } else
            {
                Console.WriteLine("Not found");
            }

        }
    }
}


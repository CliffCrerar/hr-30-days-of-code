console.log('bdd code test');

const exec = 'p6';

const functionsObject = {

    p1: function () {

        (function () {
            console.log(1);
            setTimeout(function () { console.log(2) }, 1000);
            setTimeout(function () { console.log(3) }, 0);
            console.log(4);
        })();

        var items = new Array("John", "Peter");
        console.log('items: ', items);


    },

    p2: function () {

        console.log('problem 2');



        function reverseWords(input) {
            console.log('input: ', input);

            let reversed = input.split(' ').map(w => {
                let RevWord = w.split('').reverse().join('');
                if ([',', '.', ';'].includes(RevWord[0])) {
                    const mark = RevWord[0];
                    RevWord = RevWord.substring(1, w.length-1) +mark;
                }
                return RevWord

            }).join(' ');




            return reversed

        }


        const inp = 'A helicopter is a type of rotorcraft.';


        console.log('reverseWords(inp): ', reverseWords(inp));

    },

    p3: function () {
        let s='mnonmpsms';
        console.log('problem 3');
        const ssplit = s.split("")
        console.log('split: ', ssplit);
        const strSorted = Object.assign([],ssplit).sort().join("");
        console.log('strSorted: ', strSorted);
        const regexpMatch = strSorted.match(/(.)\1+/g);
        let firstNoRepeater;
        console.log('regexpMatch: ', regexpMatch);
    
        
        let stripped = strSorted;
        console.log(stripped)
        for(let i = 0; i < regexpMatch.length ;i++){
            // stripped
            console.log(regexpMatch[i]);
            stripped = stripped.replace(regexpMatch[i],'');   
        }

        for(let i= 0;i<stripped.length;i++){
           const thisIndex = ssplit.indexOf(stripped[i])
           console.log('stripped[i]: ', stripped[i]);
           console.log('thisIndex: ', thisIndex);
           firstNoRepeater = thisIndex;
        }
        return firstNoRepeater
    },

    p4: function () {

        const pin  = '1145';
        
        console.log('-----------------------');
        console.log('-----------------------');
        console.log('problem 4');

        console.log(typeof pin);
        let valid = true;
        const checkRepeated = pin.match(/(.)\1+/g);
        console.log('checkRepeated: ', checkRepeated);
        if(pin.length > 4 || pin.length < 4){
            console.log('pin: ', pin.length);
            valid = false;
        } else if(checkRepeated!==null){
            valid = false;
        } else {
            for(let i =0;i < 4;i++){
                console.log('ch');
                console.log(pin[i]);
                console.log(Number(pin[i])+1);
                console.log(Number(pin[i+1]));
                if(Number(pin[i])+1===Number(pin[i+1])){
                    console.log('sequence');
                    valid = false;
                    break;
                }
            }
        }
        console.log(valid);
        return valid;

    },

    p5: function () {

        console.log('problem 5');
        let functions = [];
        let fn = function(num){
            this.value = num
            this[num] = ()=>this.value;
            return this.value;
        }

        for(var i = 1; i < 4; i++){
            // functions.push(function(){return i});
            functions.push(new fn(i));
        }
        console.log(functions);
        functions[1]();
        //functions.forEach((i,f)=>console.log(f['1'](),i));
    },

    p6: function(){
        let cv = 1;
        let pv = 0;
        let vc = 34;
        let passFibo = false;
        while(cv <= vc){
            const ic = cv;
            cv = pv + cv;
            pv = ic;
            if(cv === vc){
                passFibo = true;
                break;
            }
        }
        return passFibo;
    }
}

functionsObject[exec]();
console.log('functionsObject[exec]();: ', functionsObject[exec]());
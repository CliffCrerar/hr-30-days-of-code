function findHobbyists(hobbies, hobby) {
    let enjoyThisHobby = [];

    for(let person in hobbies){
        hobbies[person].includes(hobby) && enjoyThisHobby.push(person);
    }
    return enjoyThisHobby;
  }
  
  var hobbies = {
    "John": ['Piano', 'Puzzles', 'Yoga'],
    "Adam": ['Drama', 'Fashion', 'Pets'],
    "Mary": ['Magic', 'Pets', 'Reading']
  };
  
  console.log(findHobbyists(hobbies, 'Yoga'));
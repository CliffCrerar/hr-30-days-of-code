﻿using System;

namespace LeftRotation
{
    class Rotate
    {
        public static int[] Left(int[] input, int offset)
        {
            int[] newArray = new int[input.Length];

            for (int i = 0; i < input.Length; i++)
            {
                

                int newIndex = (input.Length - offset) < 0 ? (input.Length - offset) + input.Length : (input.Length - offset);
                Console.WriteLine("\n NEW INDEX "+newIndex.ToString());

                Console.WriteLine(i.ToString() + " " + (offset - 1 - i).ToString());
                newArray.SetValue(input[i],newIndex);
            }
            return newArray;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int[] arrInput = { 1, 2, 3, 4, 5 }; // input
            int Rotations = 4;

            int[] converted = Rotate.Left(arrInput, Rotations);

            foreach(int el in converted)
            {
                Console.WriteLine("\n "+el.ToString());
            }


            /*
            try
            {


                Test test = new Test();

                int[] Result = RotateFunctions.RotateArray(arrInput, 4); // running function

                bool testStatus = true; 

                // RUNNING TEST
                for(int i = 0; i < Result.Length; i++)
                {
                    if (test.resultOne[i] != Result[i])
                    {
                        testStatus = false;
                        break;
                    }
                }

                // Display test result;
                if(testStatus)
                {
                    Console.WriteLine("Test One: PASSED!");
                }
                else
                {
                    Console.WriteLine("Test One: FAILED!");
                }

            }
            catch(Exception err)
            {
                Console.WriteLine("ERROR MESSAGE: "+err.Message.ToString());
                Console.WriteLine("ERROR OCCURED: "+err.Source.ToString());
            }
            */
        }
    }
    // Function Class
    class RotateFunctions
    {
        // Main funtion
        public static int[] RotateArray(int[] a, int d)
        {
            Console.WriteLine("Rotate Left");

            int[] newArr = a;

            for (int i = 0; i < d; i++)
            {
                Console.WriteLine($"Preforming Rotation {Convert.ToString(i)} out of {Convert.ToString(d)}");
                RotateLeft(newArr);
            }
            return newArr;
        }
        // Function that preforms rotation
        static int[] RotateLeft(int[] arr)
        {
            int firstElement = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {

                if (i == arr.Length - 1)
                {
                    Console.WriteLine($"Shifting Element 0 to Element {Convert.ToString(arr.Length - 1)}");
                    //arr[i] = firstElement;
                    arr.SetValue(firstElement, arr.Length - 1);
                }
                else
                {
                    Console.WriteLine($"Shifting Element {Convert.ToString(i)} to Element {Convert.ToString(i + 1)}");
                    //arr[i] = arr[i - 1];
                    arr.SetValue(arr[i + 1], i);
                }
            }
            return arr;
        }
    }
    // Test Class
    class Test
    {
        public int[] resultOne { get; } = { 5, 1, 2, 3, 4 };
    }
}

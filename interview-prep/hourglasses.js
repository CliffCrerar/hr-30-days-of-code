// from 2d array like
/**
1 1 1 0 0 0
0 1 0 0 0 0
1 1 1 0 0 0
0 0 2 4 4 0
0 0 0 2 0 0
0 0 1 2 4 0
 */
// forms hourglasses 
/**
 a b c
   d
 e f g
 */
// determine hour glass the returns the maximum sum


'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));
    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the hourglassSum function below.
function hourglassSum(arr) {
    let arrMainLen = arr.length;
    let refRow = 0;
    // console.log(arr);
    function getHourglass(r,p){
        let sum = 0;
        const a = arr[r][p];
        const b = arr[r][p+1];
        const c = arr[r][p+2];
        const d = arr[r+1][p+1];
        const e = arr[r+2][p];
        const f = arr[r+2][p+1];
        const g = arr[r+2][p+2];
        sum = [a,b,c,d,e,f,g].reduce((acc,num)=> acc + num);
        console.log(a,b,c,d,e,f,g);
        console.log(sum);
    }
    let whileOneRuns = 0;
    let whileTwoRuns = 0;
    console.log('while 1')
    while(refRow <= arrMainLen - 3){
        console.log(arr[refRow]);
        let refPoint = 0;
        console.log('while 2')
        while(refPoint <= arr[refRow].length-3){
            const startEl = arr[refRow][refPoint];
            //console.log(startEl);
            getHourglass(refRow,refPoint)
            whileTwoRuns++
            ++refPoint;
        }
        console.log('end while 2')
        whileOneRuns++
        ++refRow;
    }
    console.log('end while 1')    
    console.log('while 1 ran:', whileOneRuns);
    console.log('while 2 ran:', whileTwoRuns);

    for( let i in arr){
        console.log('i',i,arr[i]);
    }
}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    let arr = Array(6);

    for (let i = 0; i < 6; i++) {
        arr[i] = readLine().split(' ').map(arrTemp => parseInt(arrTemp, 10));
    }

    let result = hourglassSum(arr);

    ws.write(result + "\n");

    ws.end();
}


/**
 * 
 * 0 -4 -6 0 -7 -6
-1 -2 -6 -8 -3 -1
-8 -4 -2 -8 -8 -6
-3 -1 -2 -5 -7 -4
-3 -5 -3 -6 -6 -6
-3 -6 0 -8 -6 -7
 */

 /**
  * -1 -1 0 -9 -2 -2
-2 -1 -6 -8 -2 -5
-1 -1 -1 -2 -3 -4
-1 -9 -2 -4 -4 -5
-7 -3 -3 -2 -9 -9
-1 -3 -1 -2 -4 -5
  */